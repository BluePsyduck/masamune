# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v1.1.0] - 2023-10-27

### Added

- Methods `RequestBuilder()` and `ResponseHandler()` to the `Client`.

### Changed

- Go version to 1.21.

## [v1.0.0] - 2022-19-12

### Added

- Initial implementation of the package.

[Unreleased]: https://gitlab.com/blue-psyduck/soushinki/-/compare/v1.1.0...HEAD
[v1.1.0]: https://gitlab.com/blue-psyduck/soushinki/-/compare/v1.0.0...v1.1.0
[v1.0.0]: https://gitlab.com/blue-psyduck/soushinki/-/tags/v1.0.0