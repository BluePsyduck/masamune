# Soushinki

Soushinki (送信機) is a Go package helping with sending requests to APIs and parsing the received responses back into
structs.

### Further reading:

* **[Implementing Requests](docs/Request.md)**
* **[Parsing Responses](docs/Response.md)**
* **[Using the Client](docs/Usage.md)**
* **[Testing Against a Mocked Client](docs/Testing.md)**