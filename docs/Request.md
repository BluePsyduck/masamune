# Implementing Requests

The request struct defines the data to be sent to the server including meta-level data like the request method and the
request path to use.

The definition of requests will be described using some example use-cases.

[[_TOC_]]

## Example 1: Request using path or query parameters

The following example shows a simple request, using a field as a path parameter.

```go
package example

import (
	"fmt"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"net/http"
)

type FancyRequest struct {
	request.ResponseCreator[FancyResponse]

	FancyId string
}

func (r *FancyRequest) Method() (string, error) {
	return http.MethodGet, nil
}

func (r *FancyRequest) Path() (string, error) {
	return fmt.Sprintf("/fancy/%s", r.FancyId), nil
}

type FancyResponse struct {
}
```

Embedding the `ResponseCreator` adds the `CreateResponse()` method, which is required for each request, and must provide
the response struct to use. In the case of the `ResponseCreator`, the provided type is used as response, without
additional initializations. If your response requires initializations, implement the method yourself instead of
embedding the `ResponseCreator`. In the example, the response type is `FancyResponse`, see below of more detailed
information about responses.

The `ResponseCreator` also embeds the `ResponseCaster`, which adds a `CastResponse()` method to the request to help with
casting the received response to the correct response type for the caller. While implementing `CastResponse()` is not
a strict requirement for a request, it should always be present. If you cannot use the `ResponseCreator`, embed the
`ResponseCaster` in your request struct instead.

Additionally, each request must implement the `Method()` and `Path()` methods to make it to a full request to be handled
by the `Client`. Return the values defining the request method and the path to send the request to. The path must start
with the initial slash. The host and port are part of the `Client` configuration, and must not be known by the request.

Note that this request does not specify any request body, this means it does not have one. All information is added to
the path.

## Example 2: Request with additional headers

There are use-cases, where you have to add additional headers. For this, you can add the method `Configure` to your
request. In fact, this is the last method called before the request is sent to the server, as a last instance of
manipulating everything on the low level of Go's `http.Request`.

See the following example of providing an additional header:

```go
package example

import (
	"fmt"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"net/http"
)

type FancyRequest struct {
	request.ResponseCreator[FancyResponse]

	FancyId          string
	FancyHeaderValue string
}

func (r *FancyRequest) Method() (string, error) {
	return http.MethodPost, nil
}

func (r *FancyRequest) Path() (string, error) {
	return fmt.Sprintf("/fancy/%s", r.FancyId), nil
}

func (r *FancyRequest) Configure(clientRequest *http.Request) error {
	clientRequest.Header.Set("X-Fancy-Value", r.FancyHeaderValue)
	return nil
}

type FancyResponse struct {
}
```

As mentioned above, you can do everything you want with the provided client request. The passed in request will be sent
afterwards to the server without further modifications.

## Example 3: Request with request body

If a request requires a body to be sent to the server, there are two possibilities to specify one: Either the request
provides the full body itself, or it provides a body builder which then will provide the body based on the request.

If a request can provide the request body itself, you can add the `Body()` method to the request to do so, as shown in 
the following example:

```go
package example

import (
	"bytes"
	"fmt"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"io"
	"net/http"
)

type FancyRequest struct {
	request.ResponseCreator[FancyResponse]

	FancyId      string
	FancyContent []byte
}

func (r *FancyRequest) Method() (string, error) {
	return http.MethodGet, nil
}

func (r *FancyRequest) Path() (string, error) {
	return fmt.Sprintf("/fancy/%s", r.FancyId), nil
}

func (r *FancyRequest) Body() (io.Reader, error) {
	return bytes.NewBuffer(r.FancyContent), nil
}

type FancyResponse struct {
}
```

The `Body()` method must provide the request body as a reader, and it will be used without modifications in the request 
to the server.

## Example 4: Request with JSON request body

A common use-case is to use a structured body, e.g. as JSON. Instead of marshalling the data in each request on its own,
the request can provide a request body builder instead, which will do the marshalling. In case of JSON, the package
already comes with a `JsonBodyBuilder`.

The following example shows how to use the `JsonBodyBuilder` in your request:

```go
package example

import (
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"net/http"
)

type FancyRequest struct {
	request.ResponseCreator[FancyResponse]

	FancyFoo int    `json:"fancyFoo"`
	FancyBar string `json:"fancyBar"`
}

func (r *FancyRequest) Method() (string, error) {
	return http.MethodPost, nil
}

func (r *FancyRequest) Path() (string, error) {
	return "/fancy", nil
}

func (r *FancyRequest) BodyBuilder() types.RequestBodyBuilder {
	return request.JsonBodyBuilder
}

func (r *FancyRequest) ConfigureClientRequest(clientRequest *http.Request) (*http.Request, error) {
	clientRequest.Header.Set("Content-Type", "application/json")
	return clientRequest, nil
}

type FancyResponse struct {
}
```

As you can see, we added the tags for the JSON marshaller to our request, and added the `BodyBuilder()` method, 
returning the pre-defined `JsonBodyBuilder`. You can also see that we have to add the `Configure()` method, setting the
"Content-Type" for our request, because the body builder can only provide the body, but no headers.

Because the `JsonBodyBuilder` and the corresponding `Configure` method will often be used together, the package also
provides a struct `JsonRequest` to be embedded in your request, which adds exactly these two methods:

```go
package example

import (
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"net/http"
)

type FancyRequest struct {
	request.ResponseCreator[FancyResponse]
	request.JsonRequest

	FancyFoo int    `json:"fancyFoo"`
	FancyBar string `json:"fancyBar"`
}

func (r *FancyRequest) Method() (string, error) {
	return http.MethodPost, nil
}

func (r *FancyRequest) Path() (string, error) {
	return "/fancy", nil
}

type FancyResponse struct {
}
```

With embedding the `JsonRequest` into your request, you will automatically get the marshalled JSON for your request in
the body, as well as the "Content-Type" header. Please keep in mind that if you define your own `Configure` method,
you MUST call the `JsonRequest.Configure()` method or provide the "Content-Type" header yourself.