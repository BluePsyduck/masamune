# Parsing Responses

There are actually no requirements to the response struct, you can use anything. But because a response only makes sense
if it actually holds values received from the interface, we will again discuss the abilities using some examples.

Note that the request must always provide a response struct. If the request does not require a response, use the
`EmptyResponse` provided by this package.

[[_TOC_]]

## Example 1: Response parsing the response body

There are use-cases where the response knows best how to handle the response body received from the server. For this,
add the `ParseResponse()` method to your response, which will receive the low-level `http.Response` instance:

```go
package example

import (
	"io"
	"net/http"
)

type FancyResponse struct {
	FancyContent []byte
}

func (r *FancyResponse) ParseResponse(clientResponse *http.Response) error {
	defer func() {
		_ = clientResponse.Body.Close()
	}()

	var err error
	r.FancyContent, err = io.ReadAll(clientResponse.Body)
	return err
}
```

As you can see, we are reading the body of the received response, and storing it in the response struct. In fact, the 
package provides a `RawResponse` struct, which will place the raw response in its `Content` field.

## Example 2: Response using a JSON body

Many responses will use a JSON body. The same way as the request can provide a body builder, the response is able to
provide a parser, which will parse the incoming response and write the data to the response struct. To do so, add the
`ResponseParser()` method to the response. Again, the package comes with a `JsonParser` to parse JSON responses:

```go
package example

import (
	"gitlab.com/blue-psyduck/soushinki.git/pkg/response"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
)

type FancyResponse struct {
	FancyFoo int    `json:"fancyFoo"`
	FancyBar string `json:"fancyBar"`
}

func (r *FancyResponse) ResponseParser() types.ResponseParser {
	return response.JsonParser
}
```

Using the `JsonParser` will unmarshall the received response body into the response struct, so we again define the tags 
for the JSON unmarshaller.

And the same way you can use an embedded type for the request, there is also a type to be embedded in your response to
shorten the code even further:

```go
package example

import (
	"gitlab.com/blue-psyduck/soushinki.git/pkg/response"
)

type FancyResponse struct {
	response.JsonResponse

	FancyFoo int    `json:"fancyFoo"`
	FancyBar string `json:"fancyBar"`
}
```