# Testing Against a Mocked Client

This package also comes with some logic helping with mocking the `Client`, making testing against server rather simple.

Assume we have the following function to test:

```go
package example 

import (
	"context"
	"fmt"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/client"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"net/http"
)

type FancyRequest struct {
	request.ResponseCreator[FancyResponse]

	FancyId string
}

func (r *FancyRequest) Method() (string, error) {
	return http.MethodGet, nil
}

func (r *FancyRequest) Path() (string, error) {
	return fmt.Sprintf("/fancy/%s", r.FancyId), nil
}

type FancyResponse struct {
	FancyFoo string
}

func Fancy(c *client.Client, id string) (string, error) {
	req := FancyRequest{
		FancyId: id,
	}

	res, err := req.CastResponse(c.Send(context.Background(), &req))
	if err != nil {
		return "", err
	}
	return res.FancyFoo, nil
}
```

As you can see, the function consists of building the request, handle it through the `Client` instance, and returning a
value from the received response, if no error occurred.

The test could look like this:

```go
package example

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/clienttest"
	"testing"
)

func TestFancy(t *testing.T) {
	tests := map[string]struct {
		response       *FancyResponse
		responseError  error
		expectedResult string
		expectedError  error
	}{
		"happy path": {
			response: &FancyResponse{
				FancyFoo: "foo",
			},
			responseError:  nil,
			expectedResult: "foo",
			expectedError:  nil,
		},
		"with error": {
			response:       nil,
			responseError:  fmt.Errorf("test error"),
			expectedResult: "",
			expectedError:  fmt.Errorf("test error"),
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			id := "abc"
			expectedRequest := FancyRequest{
				FancyId: "abc",
			}

			builder := clienttest.NewMockedClientBuilder()
			builder.On(&expectedRequest).Once().Return(test.response, test.responseError)

			mockedClient := builder.Client()
			result, err := Fancy(mockedClient, id)

			assert.Equal(t, test.expectedError, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}
```

The important part to look at is the `builder`: Create a new client builder with `NewMockedClientBuilder`, and start 
adding your request-response combinations to it calling `On()`. The usage of the builder is oriented on the mocks of the
`testify` package. When you added all requests and responses, you can retrieve the `client` instance using `Client()`,
and use this instead of the actual client instance. The mocked client will return the responses as you configured,
making defining any interfaces or using monkey-patching obsolete.

The builder offer basic functionality to specify how often responses should be returned on the request. Consider the
following calls on the builder:

```go
package example

import (
	"fmt"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/clienttest"
)

func testExample() {
	builder := clienttest.NewMockedClientBuilder()

	// Return the response once for the request. A second call with the same response will panic.
	builder.On(request1).Once().Return(response1, nil)

	// Return response2a on the first call, and response2b on the second call, both using an equal request. 
	// The third call with this request will panic.
	builder.On(request2).Once().Return(response2a, nil)
	builder.On(request2).Once().Return(response2b, nil)

	// Always return a response when request3a comes in, and always return an error on request3b. 
	builder.On(request3a).Return(response3, nil)
	builder.On(request3b).Return(nil, fmt.Errorf("test error"))

	// Expect the request exactly 42 times, and start returning errors from the 43rd request onwards. 
	builder.On(request4).Times(42).Return(response4, nil)
	builder.On(request4).Return(nil, fmt.Errorf("test error"))
}
```

Requests provided with `On()` are compared using `reflect.DeepEqual()`, so requests containing equal values are
considered equal, even if they do not share the same memory address. The requests are checked in order of which they are
added to the builder, and the first matching request, which still has allowed calls left, defines the response and error
to return from the mocked client.

If a more complex logic is needed to compare requests to expected ones, the method `OnMatchedBy()` can be used,
providing a callback which decides, whether the current request matches the criteria and thus the response and error
assigned to that request should be returned. See the following example:

```go
package example

import (
	"gitlab.com/blue-psyduck/soushinki.git/pkg/clienttest"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
)

type FancyRequest struct {
	Foo string
	Bar string
}

func testExample() {
	builder := clienttest.NewMockedClientBuilder()

	// Return the response, if the Foo property of the request is "foo". The value of Bar does not matter in this case.
	builder.OnMatchedBy(func(request types.Request) bool {
		return request.(*FancyRequest).Foo == "foo"
	}).Return(response, nil)
}
```

Something similar is possible for returning a response and error from the client: When using the `ReturnCallback` 
method, a callback function can be specified to dynamically generate the response and error to be returned, based on the
current request. This is especially useful when matching against multiple requests, as shown in the following example:

```go
package example

import (
	"gitlab.com/blue-psyduck/soushinki.git/pkg/clienttest"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
)

type FancyRequest struct {
	Foo string
}
type FancyResponse struct {
	Bar string
}

func testExample() {
	builder := clienttest.NewMockedClientBuilder()

	// Return the response, if the Foo property of the request is "foo". The value of Bar does not matter in this case.
	builder.OnMatchedBy(func(request types.Request) bool {
		_, ok := request.(*FancyRequest)
		return ok
	}).ReturnCallback(func(request types.Request) (types.Response, error) {
		return &FancyResponse{
			Bar: request.(*FancyRequest).Foo,
		}, nil
	})
}
```
