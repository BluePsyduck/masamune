# Using the Client

Once the requests and their responses have been implemented, you can start actually using the client to send requests
to the server.

## Create a client instance

The client is constructed with different components, which all have to be initialised and passed to the client itself.
All components, including the client, use the functional options pattern to get configured, and come with a number of 
`With*()` functions to do so.

There are three components to initialize:

* The `RequestBuilder`: This component take in your request struct, and transforms them into Go's internal 
  `http.Request` instances to be actually send to the server. It requires the base url of the server (excluding the 
  path provided by the requests themselves), and may also hold global headers to be added to all requests.
* The `ResponseHandler`: This component takes in Go's `http.Response` instances received from the server, and parses
  them into your response structures. This also includes error handling in case the server responded with a 
  non-successful status code.
* The `Client` itself: This is the main component, holding everything together. The client will be used in the project
  to actually send requests and process their responses.

The following example shows how to create the components with all available options:

```go
package example

import (
	"gitlab.com/blue-psyduck/soushinki.git/pkg/client"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/response"
	"net/http"
)

func NewFancyClient() *client.Client {
	requestBuilder := request.NewBuilder(
		request.WithBaseUrl("https://www.example.com/fancy"),
		request.WithGlobalHeaders(map[string]string{
			"X-Fancy-Key": "fancy",
		}),
	)
	responseHandler := response.NewHandler(
		response.WithDefaultErrorResponse(&response.RawErrorResponse{}),
	)

	return client.New(
		client.WithName("fancy"),
		client.WithHttpClient(http.DefaultClient),
		client.WithRequestBuilder(requestBuilder),
		client.WithResponseHandler(responseHandler),
	)
}
```

Some additional notes:
* The response handler may take an error response struct, which is a response (including all features of normal 
  responses), plus an `Error()` method to return the actual error message of the response. If no error response is set,
  the error will always be "unknown error".
* The client uses the `http.DefaultClient` as default. If you don't have an own HTTP client, you do not need to provide
  this option.
* The name of the client is only used in error messages.

## Send a request through a client

Once everything is ready, actually using the client is straightforward. Simply pass your request to the `Send()` method,
and receive your response back. 

```go
package example

import (
	"context"
	"fmt"
)

func main() {
	cl := NewFancyClient()
	
	// Set up the request instance
	req := FancyRequest{
		FancyId: "foo",
	}
	
	// Send the request to the server
	res, err := req.CastResponse(cl.Send(context.TODO(), &req))
	if err != nil {
		panic(err)
	}
	
	// Process the received response.
	fmt.Printf("%+v\n", res)
}
```

Note: Requests come with a `CastResponse()` method, which will cast the response value from the client to the actual 
response type for you. So `res` in the above example is already the correct type, without the need to cast it yourself. 