package client

import (
	"context"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/errors"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/response"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"net/http"
)

type httpClient interface {
	Do(clientRequest *http.Request) (*http.Response, error)
}

type Option func(*Client)

type Client struct {
	name            string
	httpClient      httpClient
	requestBuilder  types.RequestBuilder
	responseHandler types.ResponseHandler
}

func New(options ...Option) *Client {
	instance := Client{
		httpClient:      http.DefaultClient,
		requestBuilder:  request.NewBuilder(),
		responseHandler: response.NewHandler(),
	}

	for _, option := range options {
		option(&instance)
	}

	return &instance
}

// WithName sets the name of the client, used in error messages.
func WithName(name string) Option {
	return func(a *Client) {
		a.name = name
	}
}

// WithHttpClient sets the HTTP client to be used for sending the request. If not provided, http.DefaultClient will
// be used.
func WithHttpClient(httpClient httpClient) Option {
	return func(a *Client) {
		a.httpClient = httpClient
	}
}

// WithRequestBuilder sets the request builder to use for transforming the request structs to the HTTP requests.
func WithRequestBuilder(requestBuilder types.RequestBuilder) Option {
	return func(a *Client) {
		a.requestBuilder = requestBuilder
	}
}

// WithResponseHandler sets the response handler which will handle the responses received from the server and transforms
// them into response structs.
func WithResponseHandler(responseHandler types.ResponseHandler) Option {
	return func(a *Client) {
		a.responseHandler = responseHandler
	}
}

// RequestBuilder returns the request builder set in the client.
func (a *Client) RequestBuilder() types.RequestBuilder {
	return a.requestBuilder
}

// ResponseHandler returns the response handler set in the client.
func (a *Client) ResponseHandler() types.ResponseHandler {
	return a.responseHandler
}

// Send will send the provided request to the API, and parses the received response.
func (a *Client) Send(ctx context.Context, request types.Request) (types.Response, error) {
	clientRequest, err := a.requestBuilder.Build(request)
	if err != nil {
		return nil, errors.NewClientError(a.name, err)
	}
	clientRequest = clientRequest.WithContext(ctx)

	clientResponse, err := a.httpClient.Do(clientRequest)
	if err != nil {
		return nil, errors.NewClientError(a.name, err)
	}

	res, err := a.responseHandler.Handle(request, clientResponse)
	if err != nil {
		return nil, errors.NewClientError(a.name, err)
	}
	return res, nil
}
