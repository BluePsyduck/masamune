package client

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/errors"
	"gitlab.com/blue-psyduck/soushinki.git/test/mocks"
	"net/http"
	"testing"
)

func TestNew(t *testing.T) {
	name := "abc"
	httpClient := mocks.NewHttpClient(t)
	requestBuilder := mocks.NewRequestBuilder(t)
	responseHandler := mocks.NewResponseHandler(t)

	instance := New(
		WithName(name),
		WithHttpClient(httpClient),
		WithRequestBuilder(requestBuilder),
		WithResponseHandler(responseHandler),
	)

	assert.NotNil(t, instance)
	assert.Equal(t, name, instance.name)
	assert.Same(t, httpClient, instance.httpClient)
	assert.Same(t, requestBuilder, instance.requestBuilder)
	assert.Same(t, responseHandler, instance.responseHandler)
}

func TestClient_RequestBuilder(t *testing.T) {
	requestBuilder := mocks.NewRequestBuilder(t)

	instance := New(WithRequestBuilder(requestBuilder))
	result := instance.RequestBuilder()

	assert.Same(t, requestBuilder, result)
}

func TestClient_ResponseHandler(t *testing.T) {
	responseHandler := mocks.NewResponseHandler(t)

	instance := New(WithResponseHandler(responseHandler))
	result := instance.ResponseHandler()

	assert.Equal(t, responseHandler, result)
}

func TestClient_Send(t *testing.T) {
	type testResponse struct {
		Foo string `json:"foo"`
	}

	tests := map[string]struct {
		clientRequest     *http.Request
		requestError      error
		clientResponse    *http.Response
		clientError       error
		response          any
		responseError     error
		expectedErrorType any
	}{
		"happy path": {
			clientRequest:     &http.Request{Method: "GET"},
			requestError:      nil,
			clientResponse:    &http.Response{StatusCode: http.StatusOK},
			clientError:       nil,
			response:          &testResponse{Foo: "bar"},
			responseError:     nil,
			expectedErrorType: nil,
		},
		"with request error": {
			clientRequest:     nil,
			requestError:      fmt.Errorf("test error"),
			clientResponse:    nil,
			clientError:       nil,
			response:          nil,
			responseError:     nil,
			expectedErrorType: &errors.ClientError{},
		},
		"with client error": {
			clientRequest:     &http.Request{Method: "GET"},
			requestError:      nil,
			clientResponse:    nil,
			clientError:       fmt.Errorf("test error"),
			response:          nil,
			responseError:     nil,
			expectedErrorType: &errors.ClientError{},
		},
		"with response error": {
			clientRequest:     &http.Request{Method: "GET"},
			requestError:      nil,
			clientResponse:    &http.Response{StatusCode: http.StatusOK},
			clientError:       nil,
			response:          nil,
			responseError:     fmt.Errorf("test error"),
			expectedErrorType: &errors.ClientError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			request := mocks.NewRequest(t)

			clientRequestBuilder := mocks.NewRequestBuilder(t)
			clientRequestBuilder.On("Build", request).Once().Return(test.clientRequest, test.requestError)

			httpClient := mocks.NewHttpClient(t)
			httpClient.On("Do", mock.Anything).Maybe().Return(test.clientResponse, test.clientError)

			responseHandler := mocks.NewResponseHandler(t)
			responseHandler.On("Handle", request, test.clientResponse).Maybe().Return(test.response, test.responseError)

			instance := Client{
				requestBuilder:  clientRequestBuilder,
				httpClient:      httpClient,
				responseHandler: responseHandler,
			}
			result, err := instance.Send(context.Background(), request)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.response, result)
		})
	}
}
