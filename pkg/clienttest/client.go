package clienttest

import (
	"fmt"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/client"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"net/http"
	"reflect"
	"sync"
)

const headerKey = "key"

var globalCallIndex = 0

// RequestMatcher checks whether the provided request is matching the Call of the mocked client.
type RequestMatcher func(request types.Request) bool

func newDefaultRequestMatcher(expectedRequest types.Request) RequestMatcher {
	return func(request types.Request) bool {
		return reflect.DeepEqual(request, expectedRequest)
	}
}

// ResponseCallback is providing the response and error to return from the mocked client.
type ResponseCallback func(request types.Request) (types.Response, error)

func newDefaultResponseCallback(response any, err error) ResponseCallback {
	return func(_ types.Request) (types.Response, error) {
		return response, err
	}
}

var emptyResponseCallback = newDefaultResponseCallback(nil, nil)

type Call struct {
	client   *mockedClient
	key      string
	match    RequestMatcher
	response ResponseCallback
	times    int
}

func newCall(client *mockedClient, matcher RequestMatcher) *Call {
	globalCallIndex++
	return &Call{
		client:   client,
		key:      fmt.Sprintf("%d", globalCallIndex),
		match:    matcher,
		response: emptyResponseCallback,
		times:    -1,
	}
}

// Return sets the response and error to return for this Call of the mocked client.
func (c *Call) Return(response types.Response, err error) *Call {
	return c.ReturnCallback(newDefaultResponseCallback(response, err))
}

// ReturnCallback calls the provided callback to retrieve the response and error to return for this Call of the mocked
// client.
func (c *Call) ReturnCallback(callback ResponseCallback) *Call {
	c.client.mutex.Lock()
	defer c.client.mutex.Unlock()

	c.response = callback
	return c
}

// Times sets the number of times the mocked client should return the response. Set to -1 to allow any number of times.
func (c *Call) Times(times int) *Call {
	c.client.mutex.Lock()
	defer c.client.mutex.Unlock()

	c.times = times
	return c
}

// Once configures the Call on the mocked client to return the response once.
func (c *Call) Once() *Call {
	return c.Times(1)
}

type mockedClient struct {
	calls []*Call
	mutex sync.Mutex
}

func (c *mockedClient) Build(request types.Request) (*http.Request, error) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	call := c.findCall(request)
	if call == nil {
		panic("mocked client: no matching request found")
	}

	clientRequest := http.Request{
		Header: make(http.Header),
	}
	clientRequest.Header.Set(headerKey, call.key)
	return &clientRequest, nil
}

func (c *mockedClient) findCall(request types.Request) *Call {
	for _, call := range c.calls {
		if call.times == 0 {
			// No left-over calls
			continue
		}

		if !call.match(request) {
			// Not the request we are searching for.
			continue
		}

		if call.times > 0 {
			call.times--
		}

		return call
	}

	return nil
}

func (c *mockedClient) Do(clientRequest *http.Request) (*http.Response, error) {
	clientResponse := http.Response{
		Header: make(http.Header),
	}
	clientResponse.Header.Set(headerKey, clientRequest.Header.Get(headerKey))
	return &clientResponse, nil
}

func (c *mockedClient) Handle(request types.Request, clientResponse *http.Response) (types.Response, error) {
	key := clientResponse.Header.Get(headerKey)
	var matchedCall *Call
	for _, call := range c.calls {
		if call.key == key {
			matchedCall = call
			break
		}
	}

	return matchedCall.response(request)
}

type MockedClientBuilder struct {
	client *mockedClient
}

// NewMockedClientBuilder creates a new builder for a mocked client.
func NewMockedClientBuilder() *MockedClientBuilder {
	return &MockedClientBuilder{
		client: &mockedClient{
			calls: make([]*Call, 0),
		},
	}
}

// On creates a new call to the mocked client, expecting the provided request.
func (b *MockedClientBuilder) On(request types.Request) *Call {
	return b.OnMatchedBy(newDefaultRequestMatcher(request))
}

// OnMatchedBy creates a new call to the mocked client, calling the provided matcher to check whether a request should
// match the call.
func (b *MockedClientBuilder) OnMatchedBy(matcher RequestMatcher) *Call {
	b.client.mutex.Lock()
	defer b.client.mutex.Unlock()

	call := newCall(b.client, matcher)
	b.client.calls = append(b.client.calls, call)
	return call
}

// Client returns the mocked client instance to use, containing the added requests and responses.
func (b *MockedClientBuilder) Client() *client.Client {
	return client.New(
		client.WithName("mocked-client"),
		client.WithHttpClient(b.client),
		client.WithRequestBuilder(b.client),
		client.WithResponseHandler(b.client),
	)
}
