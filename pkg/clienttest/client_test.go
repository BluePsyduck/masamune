package clienttest

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/client"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"testing"
)

type testRequest1 struct {
	request.ResponseCreator[testResponse]

	Foo string
}

func (t *testRequest1) Method() (string, error) {
	return "TEST", nil
}

func (t *testRequest1) Path() (string, error) {
	return "/test", nil
}

type testRequest2 struct {
	request.ResponseCreator[testResponse]

	Foo string
}

func (t *testRequest2) Method() (string, error) {
	return "TEST", nil
}

func (t *testRequest2) Path() (string, error) {
	return "/test", nil
}

type testResponse struct {
	Foo string
}

func assertRequestResponse(
	t *testing.T,
	client *client.Client,
	request types.Request,
	expectedResponse types.Response,
	expectedError error,
) {
	result, err := client.Send(context.Background(), request)
	assert.Equal(t, expectedError, err)
	assert.Equal(t, expectedResponse, result)
}

func assertRequestPanics(t *testing.T, client *client.Client, request types.Request) {
	assert.Panics(t, func() {
		_, _ = client.Send(context.Background(), request)
	})
}

func TestMockedApi_Any_Single(t *testing.T) {
	request1 := testRequest1{
		Foo: "bar",
	}
	response1 := testResponse{
		Foo: "baz",
	}

	instance := NewMockedClientBuilder()
	instance.On(&request1).Return(&response1, nil)

	c := instance.Client()

	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request1, &response1, nil)
}

func TestMockedApi_Any_Multi(t *testing.T) {
	request1 := testRequest1{
		Foo: "bar",
	}
	request2 := testRequest1{
		Foo: "foo",
	}
	response1 := testResponse{
		Foo: "baz",
	}
	response2 := testResponse{
		Foo: "boo",
	}

	instance := NewMockedClientBuilder()
	instance.On(&request1).Return(&response1, nil)
	instance.On(&request2).Return(&response2, nil)

	c := instance.Client()

	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request2, &response2, nil)
	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request2, &response2, nil)
}

func TestMockedApi_Any_Similar(t *testing.T) {
	request1 := testRequest1{
		Foo: "bar",
	}
	request2 := testRequest2{
		Foo: "bar",
	}
	response1 := testResponse{
		Foo: "baz",
	}
	response2 := testResponse{
		Foo: "boo",
	}

	instance := NewMockedClientBuilder()
	instance.On(&request1).Return(&response1, nil)
	instance.On(&request2).Return(&response2, nil)

	c := instance.Client()

	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request2, &response2, nil)
	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request2, &response2, nil)
}

func TestMockedApi_Once_Single(t *testing.T) {
	request1 := testRequest1{
		Foo: "bar",
	}
	response1 := testResponse{
		Foo: "baz",
	}

	instance := NewMockedClientBuilder()
	instance.On(&request1).Once().Return(&response1, nil)

	c := instance.Client()

	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestPanics(t, c, &request1)
}

func TestMockedApi_Once_Same(t *testing.T) {
	request1 := testRequest1{
		Foo: "bar",
	}
	response1 := testResponse{
		Foo: "baz",
	}
	response2 := testResponse{
		Foo: "boo",
	}

	instance := NewMockedClientBuilder()
	instance.On(&request1).Once().Return(&response1, nil)
	instance.On(&request1).Once().Return(&response2, nil)

	c := instance.Client()

	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request1, &response2, nil)
	assertRequestPanics(t, c, &request1)
}

func TestMockedApi_Once_Multi(t *testing.T) {
	request1 := testRequest1{
		Foo: "bar",
	}
	request2 := testRequest2{
		Foo: "foo",
	}
	response1 := testResponse{
		Foo: "baz",
	}
	response2 := testResponse{
		Foo: "boo",
	}

	instance := NewMockedClientBuilder()
	instance.On(&request1).Once().Return(&response1, nil)
	instance.On(&request2).Once().Return(&response2, nil)

	c := instance.Client()

	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request2, &response2, nil)
	assertRequestPanics(t, c, &request1)
	assertRequestPanics(t, c, &request2)
}

func TestMockedApi_Times_Single(t *testing.T) {
	request1 := testRequest1{
		Foo: "bar",
	}
	response1 := testResponse{
		Foo: "baz",
	}

	instance := NewMockedClientBuilder()
	instance.On(&request1).Times(4).Return(&response1, nil)

	c := instance.Client()

	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestPanics(t, c, &request1)
}

func TestMockedApi_Times_Multi(t *testing.T) {
	request1 := testRequest1{
		Foo: "bar",
	}
	request2 := testRequest2{
		Foo: "bar",
	}
	response1 := testResponse{
		Foo: "baz",
	}
	response2 := testResponse{
		Foo: "foo",
	}

	instance := NewMockedClientBuilder()
	instance.On(&request1).Times(2).Return(&response1, nil)
	instance.On(&request2).Times(3).Return(&response2, nil)

	c := instance.Client()

	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request2, &response2, nil)
	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request2, &response2, nil)
	assertRequestPanics(t, c, &request1)
	assertRequestResponse(t, c, &request2, &response2, nil)
	assertRequestPanics(t, c, &request2)
}

func TestMockedApi_MatchedBy(t *testing.T) {
	request1 := testRequest1{
		Foo: "bar",
	}
	request2 := testRequest1{
		Foo: "foo",
	}
	response1 := testResponse{
		Foo: "baz",
	}
	response2 := testResponse{
		Foo: "boo",
	}

	instance := NewMockedClientBuilder()
	instance.OnMatchedBy(func(request types.Request) bool {
		return assert.ObjectsAreEqual("bar", request.(*testRequest1).Foo)
	}).Return(&response1, nil)
	instance.OnMatchedBy(func(request types.Request) bool {
		return assert.ObjectsAreEqual("foo", request.(*testRequest1).Foo)
	}).Return(&response2, nil)

	c := instance.Client()

	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request2, &response2, nil)
	assertRequestResponse(t, c, &request1, &response1, nil)
	assertRequestResponse(t, c, &request2, &response2, nil)
}

func TestMockedApi_ReturnCallback(t *testing.T) {
	request1 := testRequest1{
		Foo: "bar",
	}
	request2 := testRequest1{
		Foo: "foo",
	}
	expectedResponse1 := testResponse{
		Foo: "bar",
	}
	expectedResponse2 := testResponse{
		Foo: "foo",
	}

	instance := NewMockedClientBuilder()
	instance.OnMatchedBy(func(request types.Request) bool {
		_, ok := request.(*testRequest1)
		return ok
	}).ReturnCallback(func(request types.Request) (types.Response, error) {
		return &testResponse{
			Foo: request.(*testRequest1).Foo,
		}, nil
	})

	c := instance.Client()

	assertRequestResponse(t, c, &request1, &expectedResponse1, nil)
	assertRequestResponse(t, c, &request2, &expectedResponse2, nil)
	assertRequestResponse(t, c, &request1, &expectedResponse1, nil)
	assertRequestResponse(t, c, &request2, &expectedResponse2, nil)
}
