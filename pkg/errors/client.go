package errors

import "fmt"

// ClientError is the top error returned by the client.
type ClientError struct {
	clientName    string
	originalError error
}

func NewClientError(clientName string, originalError error) *ClientError {
	return &ClientError{
		clientName:    clientName,
		originalError: originalError,
	}
}

func (e *ClientError) ClientName() string {
	return e.clientName
}

func (e *ClientError) Error() string {
	return fmt.Sprintf("api %s error: %s", e.clientName, e.originalError)
}

func (e *ClientError) Unwrap() error {
	return e.originalError
}
