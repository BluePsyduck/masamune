package errors

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewError(t *testing.T) {
	clientName := "abc"
	originalError := fmt.Errorf("test error")
	expectedMessage := "api abc error: test error"

	instance := NewClientError(clientName, originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, clientName, instance.ClientName())
	assert.Equal(t, originalError, instance.Unwrap())
	assert.Equal(t, expectedMessage, instance.Error())
}
