package errors

import (
	"fmt"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
)

// RequestError is returned when an error occurred while preparing the request to be sent to the API.
type RequestError struct {
	request       types.Request
	originalError error
}

func NewRequestError(request types.Request, originalError error) *RequestError {
	return &RequestError{
		request:       request,
		originalError: originalError,
	}
}

func (e *RequestError) Request() types.Request {
	return e.request
}

func (e *RequestError) Error() string {
	return fmt.Sprintf("request error: %s", e.originalError)
}

func (e *RequestError) Unwrap() error {
	return e.originalError
}
