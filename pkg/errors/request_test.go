package errors

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blue-psyduck/soushinki.git/test/mocks"
	"testing"
)

func TestNewRequestError(t *testing.T) {
	request := mocks.NewRequest(t)
	originalError := fmt.Errorf("test error")
	expectedMessage := "request error: test error"

	instance := NewRequestError(request, originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, request, instance.Request())
	assert.Equal(t, originalError, instance.Unwrap())
	assert.Equal(t, expectedMessage, instance.Error())
}
