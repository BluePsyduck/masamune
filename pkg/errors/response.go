package errors

import (
	"fmt"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"net/http"
)

// ResponseError is returned when a response was received from the API, but it could not be handled successfully.
type ResponseError struct {
	request        types.Request
	clientResponse *http.Response
	originalError  error
}

func NewResponseError(request types.Request, clientResponse *http.Response, originalError error) *ResponseError {
	return &ResponseError{
		request:        request,
		clientResponse: clientResponse,
		originalError:  originalError,
	}
}

func (e *ResponseError) Request() types.Request {
	return e.request
}

func (e *ResponseError) ClientResponse() *http.Response {
	return e.clientResponse
}

func (e *ResponseError) Error() string {
	return fmt.Sprintf("response error: %s", e.originalError)
}

func (e *ResponseError) Unwrap() error {
	return e.originalError
}

// ResponseStatusError is returned when the API responded with a 4xx or 5xx status code.
type ResponseStatusError struct {
	statusCode     int
	request        types.Request
	clientResponse *http.Response
	originalError  error
}

func NewResponseStatusError(
	statusCode int,
	request types.Request,
	clientResponse *http.Response,
	originalError error,
) *ResponseStatusError {
	return &ResponseStatusError{
		statusCode:     statusCode,
		request:        request,
		clientResponse: clientResponse,
		originalError:  originalError,
	}
}

func (e *ResponseStatusError) StatusCode() int {
	return e.statusCode
}

func (e *ResponseStatusError) Request() types.Request {
	return e.request
}

func (e *ResponseStatusError) ClientResponse() *http.Response {
	return e.clientResponse
}

func (e *ResponseStatusError) Error() string {
	return fmt.Sprintf("request failed with status %d: %s", e.statusCode, e.originalError)
}

func (e *ResponseStatusError) Unwrap() error {
	return e.originalError
}
