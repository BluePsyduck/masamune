package errors

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blue-psyduck/soushinki.git/test/mocks"
	"net/http"
	"testing"
)

func TestNewResponseError(t *testing.T) {
	request := mocks.NewRequest(t)
	clientResponse := http.Response{}
	originalError := fmt.Errorf("test error")
	expectedMessage := "response error: test error"

	instance := NewResponseError(request, &clientResponse, originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, request, instance.Request())
	assert.Equal(t, &clientResponse, instance.ClientResponse())
	assert.Equal(t, originalError, instance.Unwrap())
	assert.Equal(t, expectedMessage, instance.Error())
}

func TestNewResponseStatusError(t *testing.T) {
	statusCode := 432
	request := mocks.NewRequest(t)
	clientResponse := http.Response{}
	originalError := fmt.Errorf("test error")
	expectedMessage := "request failed with status 432: test error"

	instance := NewResponseStatusError(statusCode, request, &clientResponse, originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, statusCode, instance.StatusCode())
	assert.Equal(t, request, instance.Request())
	assert.Equal(t, &clientResponse, instance.ClientResponse())
	assert.Equal(t, originalError, instance.Unwrap())
	assert.Equal(t, expectedMessage, instance.Error())
}
