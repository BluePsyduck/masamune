package request

import (
	"fmt"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/errors"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"io"
	"net/http"
)

type BuilderOption func(*Builder)

// Builder is the component which will transform the request struct to the HTTP request send to the server.
type Builder struct {
	baseUrl       string
	globalHeaders map[string]string
}

func NewBuilder(options ...BuilderOption) *Builder {
	instance := Builder{}

	for _, option := range options {
		option(&instance)
	}

	return &instance
}

// WithBaseUrl sets the mandatory base URL to be used in the builder. The base URL must not include a trailing slash.
func WithBaseUrl(baseUrl string) BuilderOption {
	return func(b *Builder) {
		b.baseUrl = baseUrl
	}
}

// WithGlobalHeaders sets global headers which will be added to all requests.
func WithGlobalHeaders(headers map[string]string) BuilderOption {
	return func(b *Builder) {
		b.globalHeaders = headers
	}
}

func (b *Builder) Build(request types.Request) (*http.Request, error) {
	method, err := request.Method()
	if err != nil {
		return nil, errors.NewRequestError(request, err)
	}

	path, err := request.Path()
	if err != nil {
		return nil, errors.NewRequestError(request, err)
	}
	url := fmt.Sprintf("%s%s", b.baseUrl, path)

	body, err := b.buildBody(request)
	if err != nil {
		return nil, errors.NewRequestError(request, err)
	}

	clientRequest, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, errors.NewRequestError(request, err)
	}

	for key, value := range b.globalHeaders {
		clientRequest.Header.Set(key, value)
	}

	if c, ok := request.(types.RequestConfigurator); ok {
		err = c.Configure(clientRequest)
		if err != nil {
			return nil, errors.NewRequestError(request, err)
		}
	}

	return clientRequest, nil
}

func (b *Builder) buildBody(request types.Request) (io.Reader, error) {
	if bp, ok := request.(types.RequestBodyProvider); ok {
		return bp.Body()
	}

	if p, ok := request.(types.RequestBodyBuilderProvider); ok {
		return p.BodyBuilder().BuildBody(request)
	}

	return nil, nil
}
