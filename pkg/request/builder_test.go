package request

import (
	"bytes"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/errors"
	"gitlab.com/blue-psyduck/soushinki.git/test/mocks"
	"io"
	"testing"
)

func TestNewBuilder(t *testing.T) {
	baseUrl := "abc"
	globalHeaders := map[string]string{
		"foo": "bar",
	}

	instance := NewBuilder(
		WithBaseUrl(baseUrl),
		WithGlobalHeaders(globalHeaders),
	)

	assert.NotNil(t, instance)
	assert.Equal(t, baseUrl, instance.baseUrl)
	assert.Equal(t, globalHeaders, instance.globalHeaders)
}

func TestBuilder_Build(t *testing.T) {
	tests := map[string]struct {
		url               string
		method            string
		methodError       error
		path              string
		pathError         error
		expectedErrorType any
		expectedUrl       string
	}{
		"happy path": {
			url:               "https://www.example.com",
			method:            "GET",
			methodError:       nil,
			path:              "/abc",
			pathError:         nil,
			expectedErrorType: nil,
			expectedUrl:       "https://www.example.com/abc",
		},
		"with method error": {
			url:               "https://www.example.com",
			method:            "",
			methodError:       fmt.Errorf("test error"),
			path:              "/abc",
			pathError:         nil,
			expectedErrorType: &errors.RequestError{},
			expectedUrl:       "",
		},
		"with path error": {
			url:               "https://www.example.com",
			method:            "GET",
			methodError:       nil,
			path:              "",
			pathError:         fmt.Errorf("test error"),
			expectedErrorType: &errors.RequestError{},
			expectedUrl:       "",
		},
		"with invalid request": {
			url:               "https://invalid:url",
			method:            "GET",
			methodError:       nil,
			path:              "/abc",
			pathError:         nil,
			expectedErrorType: &errors.RequestError{},
			expectedUrl:       "",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			request := mocks.NewRequest(t)
			request.On("Method").Maybe().Return(test.method, test.methodError)
			request.On("Path").Maybe().Return(test.path, test.pathError)

			instance := Builder{
				baseUrl:       test.url,
				globalHeaders: map[string]string{"foo": "bar"},
			}
			result, err := instance.Build(request)

			assert.IsType(t, test.expectedErrorType, err)
			if test.expectedUrl != "" {
				assert.Equal(t, test.expectedUrl, result.URL.String())
				assert.Equal(t, "bar", result.Header.Get("foo"))
			}
		})
	}
}

func TestBuilder_Build_WithRequestBodyProvider(t *testing.T) {
	tests := map[string]struct {
		body              io.Reader
		bodyError         error
		expectedErrorType any
		expectedBody      []byte
	}{
		"happy path": {
			body:              bytes.NewBufferString("test body"),
			bodyError:         nil,
			expectedErrorType: nil,
			expectedBody:      []byte("test body"),
		},
		"with error": {
			body:              bytes.NewBufferString("test body"),
			bodyError:         fmt.Errorf("test error"),
			expectedErrorType: &errors.RequestError{},
			expectedBody:      nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			baseUrl := "https://www.example.com"

			request := struct {
				*mocks.Request
				*mocks.RequestBodyProvider
			}{
				Request:             mocks.NewRequest(t),
				RequestBodyProvider: mocks.NewRequestBodyProvider(t),
			}
			request.Request.On("Method").Once().Return("abc", nil)
			request.Request.On("Path").Once().Return("def", nil)
			request.RequestBodyProvider.On("Body").Once().Return(test.body, test.bodyError)

			instance := Builder{
				baseUrl: baseUrl,
			}
			result, err := instance.Build(request)

			assert.IsType(t, test.expectedErrorType, err)

			if test.expectedBody != nil {
				resultBody, _ := io.ReadAll(result.Body)
				assert.Equal(t, test.expectedBody, resultBody)
			}
		})
	}
}

func TestBuilder_Build_WithRequestBodyBuilderProvider(t *testing.T) {
	tests := map[string]struct {
		body              io.Reader
		bodyError         error
		expectedErrorType any
		expectedBody      []byte
	}{
		"happy path": {
			body:              bytes.NewBufferString("test body"),
			bodyError:         nil,
			expectedErrorType: nil,
			expectedBody:      []byte("test body"),
		},
		"with error": {
			body:              bytes.NewBufferString("test body"),
			bodyError:         fmt.Errorf("test error"),
			expectedErrorType: &errors.RequestError{},
			expectedBody:      nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			requestBodyBuilder := mocks.NewRequestBodyBuilder(t)
			requestBodyBuilder.On("BuildBody", mock.Anything).Return(test.body, test.bodyError)

			request := struct {
				*mocks.Request
				*mocks.RequestBodyBuilderProvider
			}{
				Request:                    mocks.NewRequest(t),
				RequestBodyBuilderProvider: mocks.NewRequestBodyBuilderProvider(t),
			}
			request.Request.On("Method").Once().Return("abc", nil)
			request.Request.On("Path").Once().Return("def", nil)
			request.RequestBodyBuilderProvider.On("BodyBuilder").Return(requestBodyBuilder)

			instance := Builder{}
			result, err := instance.Build(&request)

			assert.IsType(t, test.expectedErrorType, err)

			if test.expectedBody != nil {
				resultBody, _ := io.ReadAll(result.Body)
				assert.Equal(t, test.expectedBody, resultBody)
			}
		})
	}
}

func TestBuilder_Build_WithConfigurator(t *testing.T) {
	tests := map[string]struct {
		configureError    error
		expectedErrorType any
		expectedResult    bool
	}{
		"happy path": {
			configureError:    nil,
			expectedErrorType: nil,
			expectedResult:    true,
		},
		"with error": {
			configureError:    fmt.Errorf("test error"),
			expectedErrorType: &errors.RequestError{},
			expectedResult:    false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			request := struct {
				*mocks.Request
				*mocks.RequestConfigurator
			}{
				Request:             mocks.NewRequest(t),
				RequestConfigurator: mocks.NewRequestConfigurator(t),
			}
			request.Request.On("Method").Once().Return("abc", nil)
			request.Request.On("Path").Once().Return("def", nil)
			request.RequestConfigurator.
				On("Configure", mock.Anything).
				Once().
				Return(test.configureError)

			instance := Builder{}
			result, err := instance.Build(request)

			assert.IsType(t, test.expectedErrorType, err)
			if test.expectedResult {
				assert.NotNil(t, result)
			} else {
				assert.Nil(t, result)
			}
		})
	}
}
