package request

import (
	"bytes"
	"encoding/json"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"io"
	"net/http"
)

// JsonRequest will marshal itself and provide the JSON data as a body for the HTTP request.
type JsonRequest struct {
}

func (r *JsonRequest) BodyBuilder() types.RequestBodyBuilder {
	return JsonBodyBuilder
}

func (r *JsonRequest) Configure(clientRequest *http.Request) error {
	clientRequest.Header.Set("Accept", "application/json")
	clientRequest.Header.Set("Content-Type", "application/json")
	return nil
}

type jsonBodyBuilder struct {
}

func (b *jsonBodyBuilder) BuildBody(request types.Request) (io.Reader, error) {
	buffer := new(bytes.Buffer)
	err := json.NewEncoder(buffer).Encode(request)
	return buffer, err
}

var JsonBodyBuilder = &jsonBodyBuilder{}
