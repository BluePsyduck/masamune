package request

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/blue-psyduck/soushinki.git/test/mocks"
	"io"
	"net/http"
	"testing"
)

func TestJsonRequest_BodyBuilder(t *testing.T) {
	instance := JsonRequest{}
	result := instance.BodyBuilder()

	assert.Equal(t, JsonBodyBuilder, result)
}

func TestJsonRequest_Configure(t *testing.T) {
	clientRequest, _ := http.NewRequest("GET", "/test", nil)

	instance := JsonRequest{}
	err := instance.Configure(clientRequest)

	assert.Nil(t, err)
	assert.Equal(t, "application/json", clientRequest.Header.Get("Accept"))
	assert.Equal(t, "application/json", clientRequest.Header.Get("Content-Type"))
}

func TestJsonBodyBuilder_BuildBody(t *testing.T) {
	type testRequest struct {
		mocks.Request
		Foo string `json:"foo"`
	}

	request := testRequest{
		Foo: "bar",
	}
	expectedResultPart := `"foo":"bar"`

	instance := jsonBodyBuilder{}
	result, err := instance.BuildBody(&request)
	resultBody, _ := io.ReadAll(result)

	assert.Nil(t, err)
	assert.Contains(t, string(resultBody), expectedResultPart)
}
