package request

import (
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/util"
)

// ResponseCaster adds the ability to cast the response to the type provided with T.
type ResponseCaster[T types.Response] struct {
}

func (ResponseCaster[T]) CastResponse(response types.Response, err error) (T, error) {
	if err != nil {
		var empty T
		return empty, err
	}

	return response.(T), nil
}

// ResponseCreator is the ability to create a new empty response instance of the provided type T.
type ResponseCreator[T types.Response] struct {
	ResponseCaster[T]
}

func (ResponseCreator[T]) CreateResponse() types.Response {
	var response T
	return util.MakeNew(response).(types.Response)
}

// ErrorResponseCreator is the ability to create a new empty error response instance of the provided type T.
type ErrorResponseCreator[T types.ErrorResponse] struct {
}

func (ErrorResponseCreator[T]) CreateErrorResponse() types.ErrorResponse {
	var response T
	return util.MakeNew(response).(types.ErrorResponse)
}
