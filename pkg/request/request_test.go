package request

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/response"
	"testing"
)

func TestResponseCaster_CastResponse(t *testing.T) {
	type testRequest struct {
		foo string
	}

	tests := map[string]struct {
		response      any
		err           error
		expectedError error
		expectedType  any
	}{
		"without error": {
			response:      &testRequest{foo: "bar"},
			err:           nil,
			expectedError: nil,
			expectedType:  &testRequest{},
		},
		"with error": {
			response:      nil,
			err:           fmt.Errorf("test error"),
			expectedError: fmt.Errorf("test error"),
			expectedType:  &testRequest{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := ResponseCaster[*testRequest]{}
			result, err := instance.CastResponse(test.response, test.err)

			assert.Equal(t, test.expectedError, err)
			assert.IsType(t, test.expectedType, result)
		})
	}
}

func TestResponseCreator_CreateResponse(t *testing.T) {
	instance := ResponseCreator[*response.RawResponse]{}
	result := instance.CreateResponse()

	assert.IsType(t, &response.RawResponse{}, result)
}

func TestErrorResponseCreator_CreateErrorResponse(t *testing.T) {
	instance := ErrorResponseCreator[*response.RawErrorResponse]{}
	result := instance.CreateErrorResponse()

	assert.IsType(t, &response.RawErrorResponse{}, result)
}
