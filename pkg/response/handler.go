package response

import (
	"gitlab.com/blue-psyduck/soushinki.git/pkg/errors"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/util"
	"net/http"
)

type HandlerOption func(*Handler)

// Handler is the component taking the HTTP response received from the server and transform it to a response struct
// as provided by the request. If an error status occurs (4xx or 5xx), it will additionally return an error.
type Handler struct {
	defaultErrorResponse types.ErrorResponse
}

func NewHandler(options ...HandlerOption) *Handler {
	instance := Handler{
		defaultErrorResponse: &UnknownErrorResponse{},
	}

	for _, option := range options {
		option(&instance)
	}

	return &instance
}

// WithDefaultErrorResponse sets the default error response struct to be used in case the response does not provide an
// own one. Whenever an error status is encountered, this error response will be used to extract a more detailed error
// from the response. If not set, "unknown error" will be used instead.
func WithDefaultErrorResponse(defaultErrorResponse types.ErrorResponse) HandlerOption {
	return func(h *Handler) {
		h.defaultErrorResponse = defaultErrorResponse
	}
}

func (h *Handler) Handle(request types.Request, clientResponse *http.Response) (types.Response, error) {
	if clientResponse.StatusCode >= 400 && clientResponse.StatusCode < 600 {
		var response types.ErrorResponse
		if erc, ok := request.(types.ErrorResponseCreator); ok {
			response = erc.CreateErrorResponse()
		} else {
			response = util.MakeNew(h.defaultErrorResponse).(types.ErrorResponse)
		}

		err := h.parseResponse(clientResponse, response)
		if err == nil {
			err = response
		}

		return response, errors.NewResponseStatusError(
			clientResponse.StatusCode,
			request,
			clientResponse,
			err,
		)
	}

	response := request.CreateResponse()
	err := h.parseResponse(clientResponse, response)
	if err != nil {
		return nil, errors.NewResponseError(request, clientResponse, err)
	}
	return response, nil
}

func (h *Handler) parseResponse(clientResponse *http.Response, response types.Response) error {
	if rp, ok := response.(types.ClientResponseParser); ok {
		return rp.ParseResponse(clientResponse)
	}

	if p, ok := response.(types.ResponseParserProvider); ok {
		return p.ResponseParser().ParseResponse(clientResponse, response)
	}

	return nil
}
