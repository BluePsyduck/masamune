package response

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/errors"
	"gitlab.com/blue-psyduck/soushinki.git/test/mocks"
	"net/http"
	"testing"
)

func TestNewHandler(t *testing.T) {
	defaultErrorResponse := &RawErrorResponse{}

	instance := NewHandler(
		WithDefaultErrorResponse(defaultErrorResponse),
	)

	assert.NotNil(t, instance)
	assert.Same(t, defaultErrorResponse, instance.defaultErrorResponse)
}

func TestHandler_Handle(t *testing.T) {
	type testResponse struct {
		Foo string
	}

	tests := map[string]struct {
		clientResponse    *http.Response
		expectedErrorType any
		expectResult      bool
	}{
		"happy path": {
			clientResponse: &http.Response{
				StatusCode: http.StatusOK,
			},
			expectedErrorType: nil,
			expectResult:      true,
		},
		"error status": {
			clientResponse: &http.Response{
				StatusCode: http.StatusBadRequest,
			},
			expectedErrorType: &errors.ResponseStatusError{},
			expectResult:      false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			response := testResponse{Foo: "bar"}

			request := mocks.NewRequest(t)
			request.On("CreateResponse").Maybe().Return(&response)

			instance := Handler{
				defaultErrorResponse: &RawErrorResponse{},
			}
			result, err := instance.Handle(request, test.clientResponse)

			assert.IsType(t, test.expectedErrorType, err)
			if test.expectResult {
				assert.Equal(t, &response, result)
			}
		})
	}
}

func TestHandler_Handle_WithClientResponseParser(t *testing.T) {
	tests := map[string]struct {
		parseError        error
		expectedErrorType any
		expectResult      bool
	}{
		"happy path": {
			parseError:        nil,
			expectedErrorType: nil,
			expectResult:      true,
		},
		"with error": {
			parseError:        fmt.Errorf("test error"),
			expectedErrorType: &errors.ResponseError{},
			expectResult:      false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			clientResponse := http.Response{
				StatusCode: http.StatusOK,
			}

			response := mocks.NewClientResponseParser(t)
			response.On("ParseResponse", &clientResponse).Return(test.parseError)

			request := mocks.NewRequest(t)
			request.On("CreateResponse").Once().Return(response)

			instance := Handler{}
			result, err := instance.Handle(request, &clientResponse)

			assert.IsType(t, test.expectedErrorType, err)
			if test.expectResult {
				assert.Equal(t, response, result)
			}
		})
	}
}

func TestHandler_Handle_WithResponseParserProvider(t *testing.T) {
	tests := map[string]struct {
		parseError        error
		expectedErrorType any
		expectResult      bool
	}{
		"happy path": {
			parseError:        nil,
			expectedErrorType: nil,
			expectResult:      true,
		},
		"with error": {
			parseError:        fmt.Errorf("test error"),
			expectedErrorType: &errors.ResponseError{},
			expectResult:      false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			clientResponse := http.Response{
				StatusCode: http.StatusOK,
			}

			response := mocks.NewResponseParserProvider(t)

			request := mocks.NewRequest(t)
			request.On("CreateResponse").Once().Return(response)

			responseParser := mocks.NewResponseParser(t)
			responseParser.On("ParseResponse", &clientResponse, response).Once().Return(test.parseError)

			response.On("ResponseParser").Once().Return(responseParser)

			instance := Handler{}
			result, err := instance.Handle(request, &clientResponse)

			assert.IsType(t, test.expectedErrorType, err)
			if test.expectResult {
				assert.Equal(t, response, result)
			}
		})
	}
}

func TestHandler_Handle_WithErrorResponseCreator(t *testing.T) {
	clientResponse := &http.Response{
		StatusCode: http.StatusBadRequest,
	}
	expectedErrorType := &errors.ResponseStatusError{}

	request := &struct {
		*mocks.Request
		*mocks.ErrorResponseCreator
	}{
		Request:              mocks.NewRequest(t),
		ErrorResponseCreator: mocks.NewErrorResponseCreator(t),
	}
	request.ErrorResponseCreator.On("CreateErrorResponse").Return(&RawErrorResponse{})

	instance := Handler{
		defaultErrorResponse: &RawErrorResponse{},
	}
	_, err := instance.Handle(request, clientResponse)

	assert.IsType(t, expectedErrorType, err)
}
