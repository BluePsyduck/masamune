package response

import (
	"encoding/json"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"net/http"
)

// JsonResponse will unmarshall the response received from the server into the current response instance.
type JsonResponse struct {
}

func (r *JsonResponse) ResponseParser() types.ResponseParser {
	return JsonParser
}

type jsonParser struct {
}

func (p *jsonParser) ParseResponse(clientResponse *http.Response, response types.Response) error {
	defer func() {
		_ = clientResponse.Body.Close()
	}()

	decoder := json.NewDecoder(clientResponse.Body)
	return decoder.Decode(response)
}

var JsonParser = &jsonParser{}
