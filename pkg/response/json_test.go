package response

import (
	"bytes"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"testing"
)

func TestJsonResponse_ResponseParser(t *testing.T) {
	instance := JsonResponse{}
	result := instance.ResponseParser()

	assert.Equal(t, JsonParser, result)
}

func TestJsonParser_ParseResponse(t *testing.T) {
	type testResponse struct {
		Foo string `json:"foo"`
	}

	tests := map[string]struct {
		body              string
		response          any
		expectedErrorType any
		expectedResponse  any
	}{
		"happy path": {
			body:              `{"foo":"bar"}`,
			response:          &testResponse{},
			expectedErrorType: nil,
			expectedResponse:  &testResponse{Foo: "bar"},
		},
		"with error": {
			body:              `{"invalid`,
			response:          &testResponse{},
			expectedErrorType: fmt.Errorf("test error"),
			expectedResponse:  &testResponse{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			clientResponse := http.Response{
				Body: io.NopCloser(bytes.NewBufferString(test.body)),
			}

			instance := jsonParser{}
			err := instance.ParseResponse(&clientResponse, test.response)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResponse, test.response)
		})
	}
}
