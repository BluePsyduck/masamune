package response

import (
	"io"
	"net/http"
)

// EmptyResponse can be used when no actual response is expected.
type EmptyResponse struct {
}

// RawResponse will store the body of the response in its Content field.
type RawResponse struct {
	Content []byte
}

func (r *RawResponse) ParseResponse(clientResponse *http.Response) error {
	if clientResponse.Body == nil {
		return nil
	}

	defer func() {
		_ = clientResponse.Body.Close()
	}()

	var err error
	r.Content, err = io.ReadAll(clientResponse.Body)
	return err
}

// UnknownErrorResponse ignores the response received from the server, and will always give "unknown error" as error
// message.
type UnknownErrorResponse struct {
}

func (r *UnknownErrorResponse) Error() string {
	return "unknown error"
}

// RawErrorResponse uses the whole body of the response as error message.
type RawErrorResponse struct {
	RawResponse
}

func (r *RawErrorResponse) Error() string {
	return string(r.Content)
}
