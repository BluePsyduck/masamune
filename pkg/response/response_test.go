package response

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"strings"
	"testing"
)

type errReader int

func (errReader) Read([]byte) (n int, err error) {
	return 0, errors.New("test error")
}

func TestRawResponse_ParseResponse(t *testing.T) {
	tests := map[string]struct {
		body            io.ReadCloser
		expectedError   error
		expectedContent []byte
	}{
		"with body": {
			body:            io.NopCloser(strings.NewReader("abc")),
			expectedError:   nil,
			expectedContent: []byte("abc"),
		},
		"without body": {
			body:            nil,
			expectedError:   nil,
			expectedContent: nil,
		},
		"with error reader": {
			body:            io.NopCloser(errReader(42)),
			expectedError:   errors.New("test error"),
			expectedContent: []byte{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			clientResponse := http.Response{
				Body: test.body,
			}

			instance := RawResponse{}
			err := instance.ParseResponse(&clientResponse)

			assert.Equal(t, test.expectedError, err)
			assert.Equal(t, test.expectedContent, instance.Content)
		})
	}
}

func TestRawErrorResponse_Error(t *testing.T) {
	content := "test error"

	instance := RawErrorResponse{
		RawResponse{
			Content: []byte(content),
		},
	}
	result := instance.Error()

	assert.Equal(t, content, result)
}

func TestUnknownErrorResponse_Error(t *testing.T) {
	instance := UnknownErrorResponse{}

	assert.Equal(t, "unknown error", instance.Error())
}