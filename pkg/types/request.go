package types

import (
	"io"
	"net/http"
)

// RequestBuilder is the ability to build the client http request to be sent to the server.
type RequestBuilder interface {
	Build(request Request) (*http.Request, error)
}

// RequestConfigurator is the ability to configure an already created client http request.
type RequestConfigurator interface {
	Configure(clientRequest *http.Request) error
}

// Request is the ability to be sent as a request through a client, providing most basic information about the request.
type Request interface {
	// Method returns the HTTP method to use for the request.
	Method() (string, error)
	// Path returns the path to use for the request, omitting the base URL and starting with a leading slash.
	Path() (string, error)
	// CreateResponse returns the struct to use for parsing the response received from the server.
	CreateResponse() Response
}

// ErrorResponseCreator is the ability to create an empty error response instance.
type ErrorResponseCreator interface {
	CreateErrorResponse() ErrorResponse
}

// RequestBodyBuilder is the ability to build the low-level body to be sent with the request.
type RequestBodyBuilder interface {
	BuildBody(request Request) (io.Reader, error)
}

// RequestBodyBuilderProvider is the ability to provide a RequestBodyBuilder.
type RequestBodyBuilderProvider interface {
	BodyBuilder() RequestBodyBuilder
}

// RequestBodyProvider is the ability to provide the low-level body to be sent with the request.
type RequestBodyProvider interface {
	Body() (io.Reader, error)
}
