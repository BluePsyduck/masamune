package types

import "net/http"

// ClientResponseParser is the ability of the response struct to parse the client response itself.
type ClientResponseParser interface {
	ParseResponse(clientResponse *http.Response) error
}

// Response is the ability to be returned from the client.
type Response interface {
}

// ErrorResponse is the ability to transform the response to an error.
type ErrorResponse interface {
	Response
	Error() string
}

// ResponseHandler is the ability to handle the response received from the server, transforming it into an object.
type ResponseHandler interface {
	Handle(request Request, clientResponse *http.Response) (Response, error)
}

// ResponseParser is the ability to parse the received http response into the provided response instance.
type ResponseParser interface {
	ParseResponse(clientResponse *http.Response, response Response) error
}

// ResponseParserProvider is the ability to provide a ResponseParser.
type ResponseParserProvider interface {
	ResponseParser() ResponseParser
}
