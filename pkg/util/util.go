package util

import "reflect"

// MakeNew creates a new instance of the provided value and returns the pointer to it.
func MakeNew[T any](value T) any {
	if t := reflect.TypeOf(value); t.Kind() == reflect.Ptr {
		return reflect.New(t.Elem()).Interface()
	}

	return new(T)
}
