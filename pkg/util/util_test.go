package util

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMakeNew_WithPointer(t *testing.T) {
	type testStruct struct {
		Foo string
	}

	value := &testStruct{}
	result := MakeNew(&testStruct{})

	assert.IsType(t, &testStruct{}, result)
	assert.NotSame(t, value, result)
}

func TestMakeNew_WithValue(t *testing.T) {
	type testStruct struct {
		Foo string
	}

	value := testStruct{}
	result := MakeNew(testStruct{})

	assert.IsType(t, &testStruct{}, result)
	assert.NotSame(t, &value, result)
}
