// Code generated by mockery. DO NOT EDIT.

package mocks

import (
	http "net/http"

	mock "github.com/stretchr/testify/mock"
)

// RequestConfigurator is an autogenerated mock type for the RequestConfigurator type
type RequestConfigurator struct {
	mock.Mock
}

// Configure provides a mock function with given fields: clientRequest
func (_m *RequestConfigurator) Configure(clientRequest *http.Request) error {
	ret := _m.Called(clientRequest)

	var r0 error
	if rf, ok := ret.Get(0).(func(*http.Request) error); ok {
		r0 = rf(clientRequest)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// NewRequestConfigurator creates a new instance of RequestConfigurator. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewRequestConfigurator(t interface {
	mock.TestingT
	Cleanup(func())
}) *RequestConfigurator {
	mock := &RequestConfigurator{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
